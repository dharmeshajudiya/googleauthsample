package com.google.test.auth.googleauthtest.web;

import com.google.test.auth.googleauthtest.service.GoogleAuthService;
import com.google.zxing.WriterException;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/auth")
@AllArgsConstructor
public class GoogleAuthController {

    GoogleAuthService googleAuthService;
    @GetMapping("secret-key")
    public String getSecretKey() {
        return googleAuthService.generateKey();
    }

    @GetMapping("authorized/{code}")
    public String authorizedUser(@PathVariable String code) {
        return googleAuthService.verifiedUser(code);
    }

    @PostMapping(value = "qr-code",
            produces = MediaType.IMAGE_PNG_VALUE)
    public  byte[]  generateQRCode(
            @RequestParam("secretKey") String secretKey,
            @RequestParam("email") String email,
            @RequestParam("companyName")String companyName) throws IOException, WriterException {
        return googleAuthService.generateQRCode(secretKey, email, companyName);
    }
}
