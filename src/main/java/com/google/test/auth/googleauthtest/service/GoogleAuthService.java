package com.google.test.auth.googleauthtest.service;

import com.google.test.auth.googleauthtest.config.Utils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class GoogleAuthService {
    String key = "";
    public String generateKey() {
        // We store this key in variable so when we authorized user we require this.
        key = Utils.generateSecretKey();
        return key;
    }
    public String verifiedUser(String code) {
        String latestCode = Utils.getTOTPCode(key);
        return latestCode.equals(code) ? "Authorized" : "Unauthorized";
    }

    public byte[] generateQRCode(String secretKey, String email, String companyName) throws IOException, WriterException {
        String barCodeUrl = Utils.getGoogleAuthenticatorBarCode(secretKey, email, companyName);
        return  createQRCode(barCodeUrl, "test.png", 400, 400);
    }


    public byte[] createQRCode(String barCodeData, String filePath, int height, int width) throws WriterException, IOException {
        BitMatrix matrix = new MultiFormatWriter().encode(barCodeData, BarcodeFormat.QR_CODE,
                width, height);
            Path path = Paths.get(filePath);
            MatrixToImageWriter.writeToPath(matrix, "png", path);
            return Files.readAllBytes(path);
    }
}
