# Getting Started

### Step to Follow
* Run GoogleAuthTestApplication.java
* Open Postman and run this GET API localhost:8080/auth/secret-key
* It generates secret key, save somewhere
* Open you Google Authenticator App Click on Plus sign
* Add secret key in key textbox and account you can give any name
* Now you are ready to go for verified
* In Postman run this GET API localhost:8080/auth/authorized/{code}
* code you need to get from your Google authenticator app
* if you give right code it verified else it will fail
