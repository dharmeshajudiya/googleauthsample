package com.google.test.auth.googleauthtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoogleAuthTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoogleAuthTestApplication.class, args);
	}

}
